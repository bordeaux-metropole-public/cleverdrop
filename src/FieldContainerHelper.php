<?php
namespace Drupal\cleverdrop;

use Drupal\Core\Security\TrustedCallbackInterface;

 /**
  * Static methods for field containers.
  */
class FieldContainerHelper implements TrustedCallbackInterface
{
  /**
   * @return string[]|void
   */
  public static function trustedCallbacks()
  {
    return ['form_action_pre_render', 'form_widget_pre_render'];
  }

  /**
   * @param $element
   * @return mixed
   */
  public static function form_action_pre_render($element) {
    unset($element['#type']);
    unset($element['#theme_wrappers']);
    $element['#theme'] = 'actions_wrapper';

    return $element;
  }

  /**
   * @param $element
   * @return mixed
   */
  public static function form_widget_pre_render($element) {
    unset($element['#type']);
    unset($element['#theme_wrappers']);
    $element['#theme'] = 'widget_wrapper';

    return $element;
  }
}
